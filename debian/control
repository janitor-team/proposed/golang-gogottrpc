Source: golang-gogottrpc
Section: devel
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Arnaud Rebillout <arnaud.rebillout@collabora.com>,
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any (>= 2:1.10~),
               golang-github-gogo-protobuf-dev (>= 0.5~),
               golang-github-pkg-errors-dev,
               golang-github-sirupsen-logrus-dev (>= 1.0.2~),
               golang-golang-x-sys-dev,
               golang-google-genproto-dev,
               golang-google-grpc-dev (>= 1.6.0~),
               golang-procfs-dev,
Rules-Requires-Root: no
Standards-Version: 4.5.0
Homepage: https://github.com/containerd/ttrpc
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-gogottrpc
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-gogottrpc.git
XS-Go-Import-Path: github.com/containerd/ttrpc,
                   github.com/stevvooe/ttrpc,
Testsuite: autopkgtest-pkg-go

Package: golang-github-containerd-ttrpc-dev
Replaces: golang-github-stevvooe-ttrpc-dev (<< 0.0~git20190211.6914432-1~),
Breaks: golang-github-stevvooe-ttrpc-dev (<< 0.0~git20190211.6914432-1~),
Provides: golang-github-stevvooe-ttrpc-dev,
Architecture: all
Depends: golang-github-gogo-protobuf-dev (>= 0.5~),
         golang-github-pkg-errors-dev,
         golang-github-sirupsen-logrus-dev (>= 1.0.2~),
         golang-golang-x-sys-dev,
         golang-google-genproto-dev,
         golang-google-grpc-dev (>= 1.6.0~),
         ${misc:Depends},
Description: GRPC for low-memory environments
 The existing grpc-go project requires a lot of memory overhead for
 importing packages and at runtime. While this is great for many services
 with low density requirements, this can be a problem when running a
 large number of services on a single machine or on a machine with a
 small amount of memory.
 .
 Using the same GRPC definitions, this project reduces the binary size
 and protocol overhead required. We do this by eliding the net/http,
 net/http2 and grpc package used by grpc replacing it with a lightweight
 framing protocol. The result are smaller binaries that use less resident
 memory with the same ease of use as GRPC.
 .
 Please note that while this project supports generating either end of
 the protocol, the generated service definitions will be incompatible
 with regular GRPC services, as they do not speak the same protocol.
 .
 This package provides sources.

Package: gogottrpc
Architecture: any
Built-Using: ${misc:Built-Using},
Depends: ${misc:Depends},
         ${shlibs:Depends},
Description: GRPC for low-memory environments - utilities
 The existing grpc-go project requires a lot of memory overhead for
 importing packages and at runtime. While this is great for many services
 with low density requirements, this can be a problem when running a
 large number of services on a single machine or on a machine with a
 small amount of memory.
 .
 Using the same GRPC definitions, this project reduces the binary size
 and protocol overhead required. We do this by eliding the net/http,
 net/http2 and grpc package used by grpc replacing it with a lightweight
 framing protocol. The result are smaller binaries that use less resident
 memory with the same ease of use as GRPC.
 .
 Please note that while this project supports generating either end of
 the protocol, the generated service definitions will be incompatible
 with regular GRPC services, as they do not speak the same protocol.
 .
 This package provides utilities.
